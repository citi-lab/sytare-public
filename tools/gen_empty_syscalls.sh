#!/bin/bash
#
# Generate empty syscalls stubs from core/syscalls.c for benchmarking the
# system without (almost) any overhead of the Sytare kernel.
#
# Note: Please refrain from any funky C syntax when declaring stuff in
#       syscalls.c. The parser uses a simple regular expression to grab all the
#       functions. In short, any line that has the substring "syt_" will be
#       treated as a syscall.


SYSCALLS_C="$(readlink -f $1)"
CORE_DIR="$(dirname ${SYSCALLS_C})"
SRC_DIR="$(dirname ${CORE_DIR})"
ROOT_DIR="$(dirname ${SRC_DIR})"
TOOLS_DIR="${ROOT_DIR}/tools"

(cd "${SRC_DIR}/include"; find drivers -type f) |
while read line
do
	echo "#include <$line>"
done

echo
echo

grep -E "syt_.+\(.*\)[^;]*\$" "$SYSCALLS_C"  | grep -v syscall | tr -d '{}' |
while read line
do
	func=`echo "$line" | sed -E "s/.*syt_([a-z0-9_]+).*/\1/g"`
	echo "$line"
	echo "{"
	echo "	__asm__ __volatile__(\"br #$func\");"
	echo "}"
	echo
done
