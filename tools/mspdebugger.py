#!/usr/bin/env python

from __future__ import print_function
import os
import subprocess
import textwrap
import re

__author__ = 'Daniel Krebs'
__email__  = 'mail@daniel-krebs.net'

class VariableNotFound(Exception):
	pass

class Msp430Debugger:

	def __init__(self, elf_file):
		if not os.path.isfile(elf_file):
			raise Exception("{} is not a file".format(elf_file))

		self.elf_file = elf_file
		self.listing_file = elf_file + '.lst'


	def _get_var_info(self, name):
		regex = re.compile('(?P<address>[a-fA-F0-9]+)\s+(?P<scope>\w+)\s+(?P<type>\w+)\s+(?P<section>[\w\._*]+)\s+(?P<size>[a-fA-F0-9]+)\s+(?P<name>.+)\s*')
		with open(self.listing_file, 'rt') as f:
			for line in f.readlines():
				match = regex.match(line)
				if (not match) or (match.group('name') != name) or (match.group('type') != 'O'):
					continue

				address = match.group('address')
				length = match.group('size')
				return (address, length)

		raise VariableNotFound()


	def get_var(self, name):

		addr, len = self._get_var_info(name)

		# run debugger to read memory
		with open(os.devnull, 'w') as devnull:
			out = subprocess.check_output(
				['mspdebug', '-q', 'rf2500', "md 0x{} 0x{}".format(addr, len)],
				stderr=devnull)

		# extract information from mspdebug output
		real_length = int(len, 16)
		lines = out.splitlines()

		var_is_number = False
		bytes_left = real_length
		data = ''

		# parse output for beginning of dumped data
		current_line = 0
		for n, line in enumerate(lines):
			if line.startswith(b'    '):
				current_line = n
				break

		while bytes_left > 0:
			line_split = lines[current_line].decode('utf-8').split()
			read_bytes = 16 if bytes_left > 16 else bytes_left

			data_current = line_split[1:(1 + read_bytes)]

			# check if we read a variable like int
			if real_length <= 4:
				var_is_number = True

				# memory on MSP430 is little endian
				data_current = reversed(data_current)

			# convert back to string
			data_current = ''.join(data_current)

			data += data_current
			bytes_left -= read_bytes
			current_line += 1

		if var_is_number:
			return (addr, len, int(data, 16))
		else:
			return (addr, len, data)


	def dump_var(self, name):
		addr, len, data = self.get_var(name)

		print('{}({}) @ 0x{}'.format(name, int(len, 16), addr.lstrip('0')), end='')

		if isinstance(data, int):
			print(' = {:d}'.format(data))
		else:
			out = ' '.join(textwrap.wrap(data, 2))
			out = textwrap.wrap(out, 16 * 2 + 15)
			i = 0
			print(':')
			for line in out:
				print('\t{:02x}: {:s}'.format(i, line))
				i += 16
