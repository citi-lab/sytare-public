#!/usr/bin/env python

import sys

# TODO: add more CLI options, like -o, and maybe some controlling switches. maybe.

# but for now, we work in-place
lstname=sys.argv[1]


symbols = dict()

# First, we locate and read the symbol table
with open(lstname) as f:
    lines = f.readlines()
    lines = [ line.strip() for line in lines ]
    symbol_table_beginning = lines.index('SYMBOL TABLE:')
    for line in lines[symbol_table_beginning+1:]:

        # a blank line marks the end of the SYMBOL TABLE
        if line == "":
            break

        address = int(line.split()[0],16)
        if address < 0x100: # filter out (most) non-address symbols
            continue
        if address > 0xff80: # filter out IVT symbols
            continue
        
        name    = line.split()[-1]
        symbols[address]=name
        
if len(symbols) == 0:
    print "Found no symbol table in file:",lstname
    sys.exit(1)

# Then, we spit out the original file, adding info where we can

sys.stdout=open(lstname,"w")
    
for line in lines:
    suffix=""

    # if line ends with an address-like pattern, try and resolve it
    if line[-6:-4] == "0x":
        address = int(line[-4:], 16)
        if address in symbols:
            suffix = "<"+symbols[address]+">"
        else:
            jumps=["jc","jge","jl","jmp","jn","jnz","jz"] #TODO: add more if needed
            if any( opcode in line for opcode in jumps):
                try:
                    for offset in range(0,300, 2):
                        if address-offset in symbols:
                            suffix = "<%s+%d>" % (symbols[address-offset], offset)
                            break

                except ValueError:
                    # some    garbage    sections   (.debug...)    get
                    # disassembled by  objdump but they make  no sense
                    # for us, so we don't waste our efforts on those
                    pass

    # resolve some more addresses into symbolic names
    if "&0x" in line:
        where=line.find("&0x")+len("&0x")
        address = int( line[where:where+4], 16 )
        
        if ( #address < 0x1000 and # only consider MMIO addresses
             address in symbols and 
             symbols[address] not in line+suffix # don't add redundant information twice
        ):
            if suffix:
                # if there was already suffix info, then we don't want to confuse the user
                suffix += " (0x%04x=&%s)" % ( address, symbols[address] )
            else:
                suffix = "(&%s)" % ( symbols[address] )
                
    if suffix:
        print line,suffix
    else:
        print line
        


