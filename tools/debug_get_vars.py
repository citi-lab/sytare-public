#!/usr/bin/env python
""" Dump variables from a running MSP430 target

This script will use the disassembly listing that is a byproduct of the build
process to extract information about the size and location of variables of the
binary to dump the requested variables from a connected MSP430 target.

Usage:
	$ ./debug_get_vars.py path/to/elf var1 var2 var3
"""

import sys
import os
import logging
from mspdebugger import Msp430Debugger

__author__ = 'Daniel Krebs'
__email__  = 'mail@daniel-krebs.net'

if __name__ == '__main__':
	try:
		debugger = Msp430Debugger(sys.argv[1])
	except IndexError:
		logging.error("No input file specified.")
		sys.exit(1)

	if len(sys.argv) <= 2:
		logging.error("No variable names specified.")
		sys.exit(1)

	# dump all variables
	var_names = sys.argv[2:]
	for var_name in var_names:
		debugger.dump_var(var_name)
