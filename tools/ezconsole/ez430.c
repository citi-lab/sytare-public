/*
 *  ez430 UART communication library.
 *  It is designed to be a userspace replacement of the
 *  buggy cdc_acm kernel driver.
 *
 *  Copyright 2013 INRIA
 *  Author : T. Pourcelot, CITI Lab
 */

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <error.h>
#include <string.h>

#include <libusb-1.0/libusb.h>

#include "ez430.h"

#ifdef DEBUG 
#define DEBUG_PRINTF(...) fprintf(stderr,__VA_ARGS__)
#else
#define DEBUG_PRINTF(...)
#endif

struct ez430_dev *ez430_open()
{
    int r;
    struct ez430_dev *dev;

    /*
     * Opening libusb
     */
    r = libusb_init(NULL);
    if (r < 0) {
        error(1, 0, "Failed to initialize libusb");
    }
#ifdef DEBUG
    libusb_set_debug(NULL, DEBUG_LEVEL);
#endif

    /*
     * Initialize device and configuration
     */
    dev = malloc(sizeof(struct ez430_dev));

    dev->ez430_handle = libusb_open_device_with_vid_pid(NULL, EZ430_VID, EZ430_PID);    
    if (dev->ez430_handle == NULL)
    {
        error(1,0,"Failed to find device matching %04x:%04x\r\n",EZ430_VID,EZ430_PID);
    }
    dev->ez430_dev = libusb_get_device(dev->ez430_handle);

    /* Get config */
    r = libusb_get_active_config_descriptor(dev->ez430_dev,&(dev->ez430_config));

    if (r != 0) // should never happen if open_device_with_vid_pid went OK
    {
        DEBUG_PRINTF("Error %d: no active config found !\r\n", r);
        exit(1);
    }

    /* We test if cdc_acm is present */
    if (libusb_kernel_driver_active(dev->ez430_handle, UART_IFACE))
    {
        error(0,0,"Detaching kernel driver");
        int x= libusb_detach_kernel_driver(dev->ez430_handle, UART_IFACE);
        if(x != 0)
        {
            error(0,0,"could not detach kernel driver: %s",libusb_error_name(x));
            error(0,0,"Please unload kernel driver manually and try again");
            exit(1);
        }
    }
    
    r = libusb_claim_interface(dev->ez430_handle, UART_IFACE);
    if (r == 0) {
    } else {
        DEBUG_PRINTF("Error claiming interface!\r\n");
        return NULL;
    }

    /* Get info on this interface endpoint */
    dev->ez430_interface = &(dev->ez430_config->interface[0].altsetting[0]);

    uint8_t my_endpoint = 0;    // TODO : refactor
    int i = 0;
    for (i = 0; i < (dev->ez430_interface)->bNumEndpoints; i++) {
        struct libusb_endpoint_descriptor my_descriptor =
            ((dev->ez430_interface)->endpoint[i]);
        int endpoint_type = my_descriptor.bmAttributes & 0x3;
        if (endpoint_type == LIBUSB_TRANSFER_TYPE_BULK) {
            if (my_descriptor.bEndpointAddress & LIBUSB_ENDPOINT_IN)
                my_endpoint = my_descriptor.bEndpointAddress;
        }
    }

    if (my_endpoint == 0) {
        DEBUG_PRINTF( "Error : cannot find a correct endpoint\r\n");
        return NULL;
    }
    dev->ez430_uart_in_endpoint = (char)my_endpoint;

    /* second endpoint : OUT */
    for (i = 0; i < (dev->ez430_interface)->bNumEndpoints; i++) {
        struct libusb_endpoint_descriptor my_descriptor =
            ((dev->ez430_interface)->endpoint[i]);
        int endpoint_type = my_descriptor.bmAttributes & 0x3;
        if (endpoint_type == LIBUSB_TRANSFER_TYPE_BULK) {
            if (!
                (my_descriptor.bEndpointAddress &
                 LIBUSB_ENDPOINT_IN))
                my_endpoint = my_descriptor.bEndpointAddress;
        }
    }
    if (my_endpoint == 0) {
        DEBUG_PRINTF("Error : cannot find a correct endpoint\r\n");
        return NULL;
    }

    dev->ez430_uart_out_endpoint = (char)my_endpoint;
    DEBUG_PRINTF("OUT endpoint adress : %x \r\n",dev->ez430_uart_out_endpoint);
    DEBUG_PRINTF("IN endpoint adress : %x \r\n", dev->ez430_uart_in_endpoint);

    return dev;
}

int ez430_close(struct ez430_dev *dev)
{

    libusb_free_config_descriptor(dev->ez430_config);

    DEBUG_PRINTF("Releasing interface\r\n");
    libusb_release_interface(dev->ez430_handle, UART_IFACE);
    DEBUG_PRINTF("Closing device\r\n");
    libusb_close(dev->ez430_handle);
    DEBUG_PRINTF("Deviced closed\r\n");
    libusb_exit(NULL);
    free(dev);
    return 0;
}

ssize_t ez430_read(struct ez430_dev * dev, void *buf, ssize_t length)
{
    int count, ret;
    // it looks like the 'length' argument is not always honored by
    // libusb_bulk_transfer(). Thus we use a local largish buffer and
    // we do an explicit check after the call.
    unsigned char dummy_buf[1000];

    ret = libusb_bulk_transfer(dev->ez430_handle,
                               dev->ez430_uart_in_endpoint,
                               dummy_buf, length, &count, TIME_OUT);

    // check that libusb did not transfer more than expected
    if(count > length)
    {
        fprintf(stderr,"libusb_bulk_transfer() error detected: %zd bytes requested, %d bytes written\r\n",
                length,count);
        count=length;
    }

    memcpy(buf,dummy_buf,count);
    
#ifdef DEBUG
    // timeouts are normal when there is nothing to receive
    if (ret != 0 && ret != LIBUSB_ERROR_TIMEOUT) {
        //if (ret != 0) { // ???
        error(0,0,"Error during USB read: %s\r",
              libusb_error_name(ret));
        fflush(stdout);
        return ret;
    }
#endif    
    if (ret != 0)
        return ret;
    return count;
}

ssize_t ez430_write(struct ez430_dev * dev, const void *buf, int length)
{
    int count, ret;
    ret = libusb_bulk_transfer(dev->ez430_handle,
                               dev->ez430_uart_out_endpoint,
                               (unsigned char *)buf, length, &count,
                               TIME_OUT);
    if (ret != 0) {
        DEBUG_PRINTF("Error during USB write : %i - %s\r\n",
                     ret, libusb_error_name(ret));
    }
    return count;
}

/*
 * utilities related to libusb
 */
static void ez430_print_config(struct libusb_config_descriptor *config);
static void ez430_print_interface_desc(const struct libusb_interface_descriptor
                       *interface);
static void ez430_print_endpoint_desc(const struct libusb_endpoint_descriptor
                      *endpoint);

static void ez430_print_dev(struct ez430_dev *dev)
{
    printf("Device struct : 0x%p\r\n", dev);
}

static void ez430_print_config(struct libusb_config_descriptor *config)
{
    printf("Config descriptor size : %i\r\n", config->bLength);
    printf("Config descriptor type : %i\r\n", config->bDescriptorType);
    printf("Number of supported interfaces : %i\r\n", config->bNumInterfaces);
}

static void ez430_print_interface_desc(const struct libusb_interface_descriptor
                       *interface)
{
    printf("Number of endpoints : %i\r\n", interface->bNumEndpoints);
    printf("USB-IF Class code : %x \r\n", interface->bInterfaceClass);
    printf("USB-IF Subclass code : %x \r\n", interface->bInterfaceSubClass);
    int i = 0;
    for (i = 0; i < interface->bNumEndpoints; i++) {
        ez430_print_endpoint_desc(&(interface->endpoint[i]));
    }
}

static void ez430_print_endpoint_desc(const struct libusb_endpoint_descriptor
                      *endpoint)
{
    printf("Size of the descriptor : %i \r\n", endpoint->bLength);
    printf("Descriptor type: %i \r\n", endpoint->bDescriptorType);
    printf("Adress of the endpoint : %i \r\n", endpoint->bEndpointAddress);
    printf("Endpoint attributes  : %i \r\n", endpoint->bmAttributes);

    printf("Endpoint Type :");
    int endpoint_type;
    endpoint_type = endpoint->bmAttributes & 0x3;

    switch (endpoint_type) {
    case LIBUSB_TRANSFER_TYPE_CONTROL:
        printf("CONTROL ENDPOINT\r\n");
        break;
    case LIBUSB_TRANSFER_TYPE_BULK:
        printf("BULK ENDPOINT\r\n");
        break;
    case LIBUSB_TRANSFER_TYPE_INTERRUPT:
        printf("INT ENDPOINT\r\n");
        break;
    case LIBUSB_TRANSFER_TYPE_ISOCHRONOUS:
        printf("ISOCHRONOUS ENDPOINT\r\n");
        break;
    default:
        printf("Unknown endpoint type!\r\n");
        break;
    }

    printf("Endpoint direction : ");
    (endpoint->bEndpointAddress & LIBUSB_ENDPOINT_IN) ? printf("OUT\r\n") :
        printf("IN\r\n");
}

void ez430_dump_info(struct ez430_dev *dev)
{
    ez430_print_dev(dev);
    ez430_print_interface_desc(dev->ez430_interface);
    ez430_print_config(dev->ez430_config); 
}
