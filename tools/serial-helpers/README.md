# Serial Helpers

The tools in this folder can help debugging Sytare via UART. Since we target
transiently powered systems, classical debugging via GDB is not always possible.
In these situations it can be of use to output debugging information via UART
without altering runtime behaviour too much.

All the tools depend on [libserialport](https://sigrok.org/wiki/Libserialport)
to access the serial port.


## Hexterm

Dump everything formatted as hexadecimal bytes to the terminal with optional
highlighting of bytes. Currently only configurable via the code, the same goes
for the baudrate (defaults to 1152000).

```bash
$ make
cc hexterm.c  -o hexterm -lserialport
cc pcsampler.c  -o pcsampler -lserialport
$ ./hexterm /dev/ttyUSB0

       0  1  2  3  4  5  6  7  8  9  a  b  c  d  e  f
0x00: 42 08 00 04 00 00 00 02 0f 40 40 40 40 40 40 40
0x10: 50 20 20 20 20 20 20 42 04 00 04 00 00 00 02 0f
0x20: 40 40 40 40 40 40 40 50 20 20 20 20 20 42
```



## PC-Sampler

This tool can be useful to monitor the current program counter of the system.
It expects 4 start bytes (0x12, 0x34, 0x56, 0x78) to synchronize and then a
stream of 2 byte wide program counter values. It uses `addr2line` from binutils
to extract more human-readable information. Baudrate is only configurable via
code.

```bash
$ make
cc hexterm.c  -o hexterm -lserialport
cc pcsampler.c  -o pcsampler -lserialport
$ ./pcsampler /dev/ttyUSB0 ../src/apps/wsn/build/app_wsn

0x0000c3b0: sensor_loop at main.c:87 (discriminator 3)
0x0000c3be: sensor_loop at main.c:89
0x0000c3c4: sensor_loop at main.c:90
```

To test your setup, you can flash the example code to the MSP430FR5739
development board like so:

```bash
$ cd msp430-pcsampler
$ make flash
```

This however requires that your toolchain can generate binaries without
explicitly supplying a linker script.
