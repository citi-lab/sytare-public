#!/bin/bash

CUR_DIR=`pwd`
TOOLS_DIR="$(readlink -f ${0%/*})"
SCHLUMPF_DIR="${TOOLS_DIR}/schlumpf"
SCHLUMPF_PC="${SCHLUMPF_DIR}/src/build/schlumpf-pc/schlumpf-pc"
APP_DIR="$(readlink -f "$1")"

# power target for 6s while programming
PROG_POWER_DURATION=6000

if [ ! -f "${APP_DIR}/CMakeLists.txt" ]
then
	echo "No valid application directory: ${APP_DIR}"
	exit 1
fi

# remove argument, so we later can pass all others by $@
shift


echo "=== Build schlumpf for pc"
cd "${SCHLUMPF_DIR}/src/pc"
scons

echo "=== Checking Schlumpf presence and version"
"$SCHLUMPF_PC" "$@" --version
case $? in
	0) ;; # all fine
	1) echo "Communication failure with Schlumpf!"; exit 1 ;;
	2) echo "WARNING: Version mismatch, you might need to flash STM32!" ;;
	*) exit 1; # some other error
esac


cd "${APP_DIR}"

# ensure fresh builds
BUILD_DIRS="build-baseline build-benchmark"
for dir in ${BUILD_DIRS}
do
	if [ -d "${dir}" ]; then
		rm -r "${dir}"
	fi

	mkdir "${dir}"
done

echo "=== Program app with nosytare kernel for baseline measuring"
cd build-baseline
cmake .. -DNOSYTARE=True
make || exit 1

# power target so that we can program it. in case of error, there's a problem
# with the user command line, so fail right away
"$SCHLUMPF_PC" --power-target=${PROG_POWER_DURATION} "$@" || exit 1

START=$SECONDS
make program || exit 1
STOP=$SECONDS

# wait until Schlumpf goes back to normal operation
sleep $(( ($PROG_POWER_DURATION / 1000) - ($STOP - $START) + 1))




# get baseline and pass user args
echo "=== Measure baseline"
SCHLUMPF_OUT=`"${SCHLUMPF_PC}" --baseline "$@" --loops=10 | tail -n 1 | grep -v Cannot`
[ -z "$SCHLUMPF_OUT" ] && exit 1
BASELINE=`echo "$SCHLUMPF_OUT" | cut -d' ' -f2`

echo "=== Program app with Sytare kernel"
cd ../build-benchmark
cmake ..
make || exit 1

# power target so that we can program it
"$SCHLUMPF_PC" --power-target=${PROG_POWER_DURATION} "$@"

START=$SECONDS
make program || exit 1
STOP=$SECONDS

# wait until Schlumpf goes back to normal operation
sleep $(( ($PROG_POWER_DURATION / 1000) - ($STOP - $START) + 1))

# go back to where we came from and run benchmark
echo "=== Run benchmark"
cd "$CUR_DIR"

GIT_DIRTY=''
if [ -n "$(git status --porcelain -uno 2> /dev/null)" ]; then GIT_DIRTY='*'; fi
GIT_HASH="$(git rev-parse --short HEAD)${GIT_DIRTY}"

"${SCHLUMPF_PC}" \
	--baseline="${BASELINE}" \
	--git-hash="${GIT_HASH}" \
	"$@"

echo
echo "Total time: $SECONDS seconds"
