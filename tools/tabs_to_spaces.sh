#!/bin/bash
# Author: Daniel Krebs
# Description: takes a list of files and convertes tabs to 4 spaces

while [ $# -gt 0 ]
do
	IN="$1"
	TMP=`mktemp`

	expand -t 4 "$IN" > "$TMP"
	mv "$TMP" "$IN"
	shift
done
