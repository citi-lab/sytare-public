#!/bin/bash

BINARY="$1"
FUNC="$2"

FUNC_SYMBOL=`msp430-elf-objdump -t "$BINARY" | grep '\.text' | grep "$FUNC"`

if [ -z "$FUNC_SYMBOL" ]; then
	echo "Function not found"
	exit 1
fi

START="0x$(echo $FUNC_SYMBOL | awk '{print $1;}')"
SIZE="0x$(echo $FUNC_SYMBOL | awk '{print $5;}')"
END="0x$(echo 16o$(($START + $SIZE - 2))p | dc)"

echo "start: $START"
echo "size:  $SIZE"
echo "end:   $END"

msp430-elf-objdump -dS --start-address=$START --stop-address=$END "$BINARY"
