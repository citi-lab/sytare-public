#!/bin/bash

OBJDUMP=msp430-elf-objdump

function get_symbol_line {
	FILE=$1
	SYMBOL=$2

	$OBJDUMP -t $FILE | grep " ${SYMBOL}\$"
}

function get_address {
	FILE=$1
	SYMBOL=$2

	get_symbol_line $FILE $SYMBOL | cut -c 8
}

function get_section {
	FILE=$1
	SYMBOL=$2

	get_symbol_line $FILE $SYMBOL | cut -c 18- | awk '{print $1}'
}

function get_size {
	FILE=$1
	SYMBOL=$2

	get_symbol_line $FILE $SYMBOL | cut -c 18- | awk '{print $2}'
}

function assert_section {
	FILE=$1
	SYMBOL=$2
	SECTION=$3

	if [ "x$(get_section $FILE $SYMBOL)" = "x$SECTION" ]; then
		return 0
	else
		echo "Symbol '$SYMBOL' not in section '$SECTION'"
		exit 1
	fi
}
