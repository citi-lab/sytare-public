#!/usr/bin/env python
""" Assert the content of a variable on a running MSP430 target

Usage:
	$ ./assert_var.py path/to/elf NAME CONDITION CONTENT

Example:
	$ ./assert_var.py basic_test stats_boot_count >= 10
"""

import sys
import os
import logging
from mspdebugger import Msp430Debugger

__author__ = 'Daniel Krebs'
__email__  = 'mail@daniel-krebs.net'

if __name__ == '__main__':
	try:
		debugger = Msp430Debugger(sys.argv[1])
	except IndexError:
		logging.error("No input file specified.")
		sys.exit(1)

	if len(sys.argv) <= 4:
		logging.error("Not enough arguments")
		sys.exit(1)

	var_name = sys.argv[2]
	condition = sys.argv[3]
	content = sys.argv[4]

	addr, len, data = debugger.get_var(var_name)

	if isinstance(data, str):
		# wrap with '
		data = "'{}'".format(data)
		content = "'{}'".format(content)

	expression = '{} {} {}'.format(data, condition, content)

	if not eval(expression):
		print("Variable '{}' does not match!".format(var_name))
		print('\tExpected: {:2s} {}'.format(condition, content))
		print('\tGot:         {}'.format(data))
		sys.exit(1)

	sys.exit(0)
