// GS-2016-07-21 copy-pasting the original ez430 BSP

// notable differences:
//
// - Interrupt Service Routines now  use the GCC/CGT attribute syntax,
//   cf SLAU646A §7.2 and SLAU132L §5.11.18
//
// - requesting for the  MCU to stay awake after servicing  the ISR is
//   now done via GCC/CGT intrinsic, cf SLAU646A §7.1 and SLAU132L§6.8.1


#include <msp430.h>
#include "timer.h"


// redefining stuff because we don't want to mess with newlib
#define NULL ((void*)0)

/*********************** copy-paste starts here ************************/

/**
 *  \file   timer.c
 *  \brief  eZ430-RF2500 tutorial, timers
 *  \author Antoine Fraboulet, Tanguy Risset, Dominique Tournier
 *  \date   2009
 **/


/* ************************************************** */
/* TimerA on VLO 12kHz                                */
/* ************************************************** */

static volatile timer_cb timerA_cb;
static volatile int timerA_wakeup;

//ISR(TIMERA0, Timer_A) // GS-2016-07-21
void __attribute__ ((interrupt(TIMERA0_VECTOR))) timerA_isr(void)
{
	if (timerA_cb != NULL)
		timerA_cb();

	if (timerA_wakeup == 1)
		__low_power_mode_off_on_exit();
		// LPM_OFF_ON_EXIT;  GS-2016-07-21
}

void timerA_init(void)
{
	timerA_cb = NULL;
	timerA_wakeup = 0;
	timerA_stop();
}

void timerA_register_cb(timer_cb cb)
{
	timerA_cb = cb;
}

void timerA_set_wakeup(int w)
{
	timerA_wakeup = w;
}

void timerA_start_ticks(unsigned ticks)
{
	BCSCTL3 |= LFXT1S_2;	// LFXT1 = VLO
	TACCTL0 = CCIE;		// TCCR0 interrupt enabled
	TAR = 0;
	TACCR0 = ticks;
	TACTL = TASSEL_1 + MC_1;	// ACLK, upmode
}

#define VLO_FREQ 12000
#define TICKS_IN_MS (VLO_FREQ/1000)

void timerA_start_milliseconds(unsigned ms)
{
	timerA_start_ticks(ms * TICKS_IN_MS);
}

void timerA_stop(void)
{
	TACTL = 0;
}

/* ************************************************** */
/* TimerB on VLO 12kHz                                */
/* ************************************************** */

static volatile timer_cb timerB_cb;
static volatile int timerB_wakeup;

// GS-2016-07-21
void __attribute__ ((interrupt(TIMERB0_VECTOR))) timerB_isr(void)
{
	if (timerB_cb != NULL)
		timerB_cb();

	if (timerB_wakeup == 1)
        // GS-2016-07-21
		__low_power_mode_off_on_exit();
}

void timerB_init(void)
{
	timerB_cb = NULL;
	timerB_wakeup = 0;
	timerB_stop();
}

void timerB_register_cb(timer_cb cb)
{
	timerB_cb = cb;
}

void timerB_set_wakeup(int w)
{
	timerB_wakeup = w;
}

void timerB_start_ticks(unsigned ticks)
{
	BCSCTL3 |= LFXT1S_2;	// LFXT1 = VLO
	TBCCTL0 = CCIE;		// TCCR0 interrupt enabled
	TBR = 0;
	TBCCR0 = ticks;
	TBCTL = TBSSEL_1 + MC_1;	// ACLK, upmode
}

void timerB_start_milliseconds(unsigned ms)
{
	timerB_start_ticks(ms * TICKS_IN_MS);
}

void timerB_stop(void)
{
	TBCTL = 0;
}

/* ************************************************** */
/*                                                    */
/* ************************************************** */
