#ifndef UART_H
#define UART_H

/* ************************************************** */
/* UART                                               */
/* ************************************************** */

#define UART_9600_SMCLK_1MHZ   0x01
#define UART_9600_SMCLK_8MHZ   0x08

// uart callback function type: 
typedef int (*uart_cb_t) (unsigned char data);

void uart_init(int config);
void uart_stop(void);
void uart_register_cb(uart_cb_t);

int uart_putchar(int c);
int uart_getchar();

void uart_print(char* s);

/* ************************************************** */
/*                                                    */
/* ************************************************** */

#endif
