#ifndef LEDS_H
#define LEDS_H

void leds_init(void);
void leds_on(void);
void leds_off(void);
void led_green_off(void);
void led_green_on(void);
void led_green_switch(void);
void led_red_off(void);
void led_red_on(void);
void led_red_switch(void);

#endif
