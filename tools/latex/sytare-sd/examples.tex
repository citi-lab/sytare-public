\documentclass[a4paper,11pt, svgnames]{article}

\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}

%%%%%%%%%%%%%%% comment/uncomment these lines to select non-default fonts and check whether everything still looks correct

%% BEGIN toggle font 1
% \usepackage[scaled=.95]{helvet}
% \renewcommand{\familydefault}{\sfdefault}
% \usepackage[EULERGREEK]{sansmath}\sansmath % sans-serif math (including greeks)
%% END toggle font 1

%% BEGIN toggle font 2
% \usepackage[sfdefault]{classico}
% \usepackage[EULERGREEK]{sansmath}\sansmath % sans-serif math (including greeks)
%% END toggle font 2



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Layout

\usepackage[margin=2cm]{geometry}
\setlength{\parindent}{0pt}

\usepackage{enumitem}
\setlist{nosep}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
% Utilities and misc

\usepackage{needspace}

\usepackage{sytare-sd}

\usepackage[
]{showexpl}
\lstset{%
  explpreset={
    language=[LaTeX]TeX,
    frame=single,
    columns=fullflexible,
    basicstyle=\ttfamily\small,
    keepspaces=true,
  }
}

\begin{document}


\section*{Sytare sequence diagrams}

This \texttt{sytare-sd} package is a collection of macros for drawing pseudo-UML sequence diagrams. 
%
It is based very much on the \texttt{pgf-umlsd} package but allows for more fine-grained control.

\section{Basic examples}

\subsection{Empty diagram}

Everything in this package must happen within the \texttt{sequencediagram} environment. 
%
In practice, this is just a \texttt{tikzpicture} with some boilerplate.

\begin{LTXexample}
  \begin{sequencediagram}
  \end{sequencediagram}
\end{LTXexample}

\subsection{Lifelines} 

Lifelines for entities are created with the \verb+\lifeline+ command. 
%
The first argument must be a valid tikz node name. 
%
The second argument will be typeset in a rectangle box.


By default, there is a 1em distance between two boxes, but using the optional argument allows for manual control.

\begin{LTXexample}
\begin{sequencediagram}
  \lifeline{bla}{Bla}
  \lifeline{ble}{Ble}
  \lifeline[2em]{bli}{Bli}
  \lifeline[8em]{blo}{Blo}
  \advancetime{20}
\end{sequencediagram}
\end{LTXexample}

Remarks:
\begin{itemize}
\item Note that some \verb+\advancetime{delta}+ is required to create any vertical space at all. 
%
The package never changes the current vertical position by itself.
\item Unlike \texttt{pgf-umlsd.sty} we do not distinguish between active ``threads'' and passive ``instances''.
\end{itemize}

\subsection{Function calls} 

Function calls and other types of control structures must be drawn from lower-level primitives: 
\begin{itemize}
\item the grey rectangles are obtained via a combination of \verb+\activate{line}+, \verb+\advancetime{delta}+, and \verb+\deactivate{line}+
\item the arrows are obtained via the \verb+\arrow[optional text]{from}{to}+ command.
\end{itemize}

\medskip

\begin{LTXexample}
\begin{sequencediagram}
  \lifeline{bla}{Bla}
  \lifeline[2em]{ble}{Ble}
  \activate{bla}
  \advancetime{15}
  \deactivate{bla}
  \arrow[func()]{bla}{ble}
  {
    \activate{ble}
    \advancetime{10}
    \deactivate{ble}
    \arrow{ble}{bla}
  }
  \activate{bla}
  \advancetime{10}
  \deactivate{bla}
\end{sequencediagram}
\end{LTXexample}


Remarks:
\begin{itemize}
\item Scale on the vertical axis is harcoded to  1em = 10 time units.
\item The curly braces in the example are not meaningful but they help with readability
\item Unlike \texttt{pgf-umlsd.sty} we have no concept of ``call self'' (as of now ;  we may add it later if needed)
\end{itemize}




\subsection{Annotations on the diagram}

The package has built-in support for bullet markers (i.e. \textbullet), side notes (on both sides), and dashed horizontal lines.

\begin{LTXexample}
\begin{sequencediagram}
  \lifeline{bla}{Bla}
  \lifeline{ble}{Ble}
  \lifeline[6em]{bli}{Bli}
  \advancetime{10}
  \marker{bli}
  \advancetime{10}
  \activate{bla}
  \advancetime{10}
  \marker{bla}
  % raisebox to move away from dashed line
  \noteleft{bla}{\raisebox{3ex}{t$_0$}} 
  \horizontalline{bla}{bli}
  \noteright[2.5em]{bli}{\textcircled{a}}
  \deactivate{bla}
  \advancetime{10}
  \marker{ble}
  \noteright{ble}{t$_1$=t$_0+\Delta$t}
  \advancetime{20}
\end{sequencediagram}
\end{LTXexample}

\section{Advanced examples}


\subsection*{Names with unusual shapes}

Wide names work out-of-the-box. 
%
However high (e.g. multiline) or deep (below the baseline) names cause node alignement problems which I don't know how to resolve (cf beamer manual §5.2 ``Aligning the Nodes Using Positioning Options'') so they are forbidden. 


\begin{LTXexample}
\begin{sequencediagram}
  \lifeline{a}{1\raisebox{3ex}{2}3}
  \lifeline{c}{Desinstitutionnalisation}
  \lifeline{aa}{\raisebox{-3ex}{1}
                2
                \raisebox{-3ex}{3}}
\end{sequencediagram}
\end{LTXexample}

Note: thus far we hardcode the height and depth of labels to enforce that all lifelines are aligned vertically.
%
Otherwise our coordinate calcuations are wrong, which results in horizontal lines and hotizontal arrows to be not horizontal at all.

\subsection*{Arbitrary paths among lifelines}

Commands \verb+\activate+ and \verb+\deactivate+ must come in well-formed pairs or else activation rectangles will be drawn incorrectly.
%
However arrows have no such constraints. 
%
Also, \verb+\advancetime+ can be used with a negative argument to rewind time.
%
This allows for arbitrary diagrams which make no particular sense in terms of control flow. 

\begin{LTXexample}
\begin{sequencediagram}
  \lifeline{a}{A}
  \lifeline{b}{B}
  \lifeline{c}{C}
  \activate{a}
  \advancetime{15}
  \deactivate{a}
  \arrow{a}{c}
  \advancetime{-10}
  \activate{c}
  \advancetime{20}
  \deactivate{c}
  \advancetime{-5}
  \arrow{c}{b}
  \activate{b}
  \advancetime{5}
  \arrow{a}{b}
  \advancetime{10}
  \deactivate{b}
  \advancetime{5}
\end{sequencediagram}
\end{LTXexample}

Remarks
\begin{itemize}
\item Like activation rectangles, our lifelines really are tikz nodes with an empty text.
%
 As such, they are about .66em wide, i.e. twice the default ``inner sep'' length.
 %
 This is why the arrow from A to B doesn't start from the dotted line: it correctly starts from the edge of the (undrawn) node.
\end{itemize}
\subsection*{Asynchronous events}

\begin{LTXexample}
\begin{sequencediagram}
  \lifeline{a}{App}
  \lifeline[3em]{b}{OS}
  \advancetime{10}
  \event[boot]{b}
  \activate{b}
  \advancetime{20}
  \deactivate{b}
  \arrow[resume]{b}{a}
  \activate{a}
  \advancetime{20}
  % IRQ e.g. power failure detection
  \event[IRQ]{a}
  \deactivate{a}
  \arrow{a}{b}
  \activate{b}
  \advancetime{10}
  \deactivate{b}
  \bigcross{b}
\end{sequencediagram}
\end{LTXexample}



\needspace{.5\paperheight}
\subsection*{Using tikz features from within a sequence diagram}


Please suggest tikz-oriented improvements, e.g. styling diagram elements, adding extra nodes, etc.

\begin{LTXexample}[pos=a,basicstyle=\ttfamily\footnotesize]
\begin{sequencediagram}
  \lifeline{app}{Application}
  \lifeline[5em]{os}{OS layer}
  \lifeline[4em]{da}{Driver A}
  \lifeline[3em]{db}{Driver B}
  \lifeline{hw}{Hardware}
  \activate{app}
  \advancetime{30}
  \deactivate{app}   \arrow[syt\_rf\_send()]{app}{os}   \activate{os}
  \advancetime{10}
  \deactivate{os}   \arrow[rf\_send()]{os}{da}   \activate{da}
  \advancetime{15}

  \foreach \i in {1,2,...,5}
  {
    \deactivate{da} \arrow[\small spi\_strobe()]{da}{db} \activate{db}
    \advancetime{2}
    {
      \arrow{db}{hw} \activate{hw} \advancetime{3}
      \noteright{hw}{\scriptsize r/w}
      \advancetime{3} 
      \deactivate{hw} \arrow{hw}{db} 
    }
    \advancetime{2}
    \deactivate{db} \arrow{db}{da} \activate{da}

    \advancetime{15}      % 15 is just to leave room for the spi_strobe() label
  }
  \deactivate{da}   \arrow{da}{os}  \activate{os}
  \advancetime{10}
  \deactivate{os}   \arrow{os}{app}  \activate{app}
  \advancetime{30}
  \deactivate{app} 

  \bigcross{app}
  \noteright{app}{power failure}
\end{sequencediagram}
\end{LTXexample}

\end{document}
