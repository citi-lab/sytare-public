/**
 *  \file   kernel.h
 *  \brief  Sytare project os header file : internals definitions
 *  \author Tristan Delizy
 *  \date   2015
 **/

#ifndef SYTARE_OS_H
#define SYTARE_OS_H

/******************************************************************/
/****************************************************** INCLUDES **/

#include <stdint.h>
#include <stddef.h>

#include "config.h"
#include <drivers/utils.h>

#include <drivers/clock.h>
#include <drivers/spi.h>
#include <drivers/cc2500.h>
#include <drivers/temperature.h>
#include <drivers/port.h>

/******************************************************************/
/************************************************ SYSTEM DEFINES **/

#define SYT_LOW_LEVEL_DBG       1       /* Set the usage of debug pin/bit for signaling system checkpoint operation*/
#define SYT_DBG_PIN             1
#define SYT_DBG_BIT             4

#define SYT_HRST_PIN_1          2       /* system hard reset pin*/
#define SYT_HRST_BIT_1          5       /* system hard reset bit*/
#define SYT_HRST_PIN_2          2       /* system hard reset pin*/
#define SYT_HRST_BIT_2          6       /* system hard reset bit*/

// Signalize app termination via GPIO: low->high when main() terminates
#define SYT_FINISHED_SIG_PORT       1
#define SYT_FINISHED_SIG_BITMASK    (1 << 2)

// Read GPIO pin on boot and reset progress if level is high
#define SYT_RESET_SIG_PORT          1
#define SYT_RESET_SIG_BITMASK       (1 << 5)


/******************************************************************/
/******************************************************** MACROS **/

#if SYT_LOW_LEVEL_DBG == 1
#define SYT_DBG_PIN_HIGH()      do { PXOUT(SYT_DBG_PIN) |= (BITX(SYT_DBG_BIT)); } while(0)
#define SYT_DBG_PIN_LOW()       do { PXOUT(SYT_DBG_PIN) &= ~(BITX(SYT_DBG_BIT)); } while(0)
#else
#define SYT_DBG_PIN_HIGH()      do { } while(0)
#define SYT_DBG_PIN_LOW()       do { } while(0)
#endif


/******************************************************************/
/***************************************************** VARIABLES **/

// stack pointers
extern size_t syt_os_sp;
extern size_t syt_usr_sp;

// syscall checkpoint pointer
extern size_t syt_syscall_ptr;


/******************************************************************/
/************************************************ STACK ROUTINES **/

// always inline because a "ret" after a stack change only have sense in the last function called
// in the other stack. in other terms, the function switching to OS stack MUST restore USR stack
// before it's end. an exception would be the startup, where the main can be called explicitelly
// the user stack being not yet used.
inline __attribute__((always_inline))
static void syt_run_os_stack(void)
{
    // save USR stack pointer and change stack pointer USR -> SYS
    __asm__ __volatile__(   "mov r1, %0 \n\t"
                            "mov %1, r1\n\t"
                            : "=m" (syt_usr_sp)
                            : "m" (syt_os_sp)
                            );
}

// always inline because a "ret" after a stack change only have sense in the last function called
// in the other stack. in other terms, the function switching to OS stack MUST restore USR stack
// before it's end. an exception would be the startup, where the main can be called explicitelly
// the user stack being not yet used.
inline __attribute__((always_inline))
static void syt_run_usr_stack(void)
{
    // save USR stack pointer and change stack pointer SYS -> USR
    __asm__ __volatile__(   "mov r1, %0 \n\t"
                            "mov %1, r1\n\t"
                            : "=m" (syt_os_sp)
                            : "m" (syt_usr_sp)
                            );
}

#endif
