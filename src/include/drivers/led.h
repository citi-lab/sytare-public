/**
 *  \file   led.h
 *  \brief  TI FR5739 board lib, leds
 *  \author Tristan Delizy
 *  \date   2015
 *  \from   eZ430-RF2500 tutorial, leds
 *          (Antoine Fraboulet, Tanguy Risset,
 *          Dominique Tournier, Sebastien Mazy)
 **/

#ifndef LEDS_H
#define LEDS_H


/******************************************************************/
/******************************************************* DEFINES **/

#define LED_1           BIT0
#define LED_2           BIT1
#define LED_3           BIT2
#define LED_4           BIT3
#define LED_5           BIT4
#define LED_6           BIT5
#define LED_7           BIT6
#define LED_8           BIT7


/******************************************************************/
/***************************************************** FUNCTIONS **/

/*
 * /!\ led numbering start at 1
 * and there is 8 leds on the board.
 */

void leds_init(void);
void leds_on(void);
void leds_off(void);
void led_off(unsigned int led_num);
void led_on(unsigned int led_num);
void led_switch(unsigned int led_num);

#endif
