/**
 *  \file   drivers/port.h
 *  \brief  TI FR5739 board lib, ports (GPIO and pin functions)
 *  \author Tristan Delizy
 *  \date   2016
 **/

#ifndef PRT_H
#define PRT_H

/******************************************************************/
/****************************************************** SYSCALLS **/

// /!\ port J is attended as "5" in the functions taking an int port arg.

void syt_prt_drv_init(void);

void syt_prt_out_bis(unsigned char port, unsigned char mask);
void syt_prt_out_bic(unsigned char port, unsigned char mask);
unsigned char syt_prt_out_get(unsigned char port);

unsigned char syt_prt_in_get(unsigned char port);

void syt_prt_dir_bis(unsigned char port, unsigned char mask);
void syt_prt_dir_bic(unsigned char port, unsigned char mask);
unsigned char syt_prt_dir_get(unsigned char port);

void syt_prt_sel0_bis(unsigned char port, unsigned char mask);
void syt_prt_sel0_bic(unsigned char port, unsigned char mask);
unsigned char syt_prt_sel0_get(unsigned char port);

void syt_prt_sel1_bis(unsigned char port, unsigned char mask);
void syt_prt_sel1_bic(unsigned char port, unsigned char mask);
unsigned char syt_prt_sel1_get(unsigned char port);

void syt_prt_ie_bis(unsigned char port, unsigned char mask);
void syt_prt_ie_bic(unsigned char port, unsigned char mask);
unsigned char syt_prt_ie_get(unsigned char port);

void syt_prt_ies_bis(unsigned char port, unsigned char mask);
void syt_prt_ies_bic(unsigned char port, unsigned char mask);
unsigned char syt_prt_ies_get(unsigned char port);

void syt_prt_ifg_bis(unsigned char port, unsigned char mask);
void syt_prt_ifg_bic(unsigned char port, unsigned char mask);
unsigned char syt_prt_ifg_get(unsigned char port);

void syt_prt_ren_bis(unsigned char port, unsigned char mask);
void syt_prt_ren_bic(unsigned char port, unsigned char mask);
unsigned char syt_prt_ren_get(unsigned char port);


/******************************************************************/
/***************************************************** FUNCTIONS **/
void prt_drv_init(void);
void prt_drv_restore(int handle);
void prt_drv_save(int handle);

void prt_out_bis(unsigned char port, unsigned char mask);
void prt_out_bic(unsigned char port, unsigned char mask);
unsigned char prt_out_get(unsigned char port);

unsigned char prt_in_get(unsigned char port);

void prt_dir_bis(unsigned char port, unsigned char mask);
void prt_dir_bic(unsigned char port, unsigned char mask);
unsigned char prt_dir_get(unsigned char port);

void prt_sel0_bis(unsigned char port, unsigned char mask);
void prt_sel0_bic(unsigned char port, unsigned char mask);
unsigned char prt_sel0_get(unsigned char port);

void prt_sel1_bis(unsigned char port, unsigned char mask);
void prt_sel1_bic(unsigned char port, unsigned char mask);
unsigned char prt_sel1_get(unsigned char port);

void prt_ie_bis(unsigned char port, unsigned char mask);
void prt_ie_bic(unsigned char port, unsigned char mask);
unsigned char prt_ie_get(unsigned char port);

void prt_ies_bis(unsigned char port, unsigned char mask);
void prt_ies_bic(unsigned char port, unsigned char mask);
unsigned char prt_ies_get(unsigned char port);

void prt_ifg_bis(unsigned char port, unsigned char mask);
void prt_ifg_bic(unsigned char port, unsigned char mask);
unsigned char prt_ifg_get(unsigned char port);

void prt_ren_bis(unsigned char port, unsigned char mask);
void prt_ren_bic(unsigned char port, unsigned char mask);
unsigned char prt_ren_get(unsigned char port);

#endif
