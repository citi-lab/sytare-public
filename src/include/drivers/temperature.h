/**
 *  \file   drivers/temp.h
 *  \brief  TI FR5739 board lib, dma
 *  \author Tristan Delizy
 *  \date   2016
 **/

#ifndef TMP_H
#define TMP_H

/******************************************************************/
/****************************************************** SYSCALLS **/
void syt_tmp_drv_init(void);

int syt_tmp_drv_sample(void);


/******************************************************************/
/***************************************************** FUNCTIONS **/
void tmp_drv_init(void);
void tmp_drv_restore(int drv_handle);
void tmp_drv_save(int drv_handle);

int tmp_drv_sample(void);

#endif
