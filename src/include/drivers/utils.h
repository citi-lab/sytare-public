/**
 *  @brief  Helpers for driver implementation
 *  \author Tristan Delizy, Daniel Krebs
 **/


#ifndef DRIVERS_UTILS_H
#define DRIVERS_UTILS_H

#include <stdint.h>
#include <stddef.h>
#include <msp430.h>

/******************************************************************/
/************************************************* UTILS DEFINES **/

inline static int max_int ( int a, int b ) { return (a) > (b) ? (a) : (b); }
inline static int min_int ( int a, int b ) { return (a) < (b) ? (a) : (b); }
inline static int round_div_int ( int a, int b ) { return ((a) + (b) / 2) / (b); }

inline static unsigned int max_uint ( unsigned int a, unsigned int b ) { return (a) > (b) ? (a) : (b); }
inline static unsigned int min_uint ( unsigned int a, unsigned int b ) { return (a) < (b) ? (a) : (b); }


#define ALL_PINS (0xFF) // for an 8 bits registry

#define _PXOUT(n) P##n##OUT
#define _PXIN(n)  P##n##IN
#define _PXDIR(n) P##n##DIR
#define _PXSEL0(n) P##n##SEL0
#define _PXSEL1(n) P##n##SEL1
#define _PXIE(n)  P##n##IE
#define _PXIES(n) P##n##IES
#define _PXIFG(n) P##n##IFG
#define _PXREN(n) P##n##REN
#define _BITX(n) BIT##n

#define PXOUT(n) _PXOUT(n)
#define PXIN(n) _PXIN(n)
#define PXDIR(n) _PXDIR(n)
#define PXSEL0(n) _PXSEL0(n)
#define PXSEL1(n) _PXSEL1(n)
#define PXIE(n) _PXIE(n)
#define PXIES(n) _PXIES(n)
#define PXIFG(n) _PXIFG(n)
#define PXREN(n) _PXREN(n)
#define BITX(n) _BITX(n)


/******************************************************************/
/********************************************** DRIVER INTERFACE **/

/// Function pointer for driver save function
typedef void (*drv_save_func_t)(int handle);

/// Function pointer for driver restore function
typedef void (*drv_restore_func_t)(int handle);


/**
 * @brief Register driver to the kernel for persistency
 *
 * Note: Has to be a macro because the ctx_size must be known at compile time,
 *       it cannot be a function argument (even inlined).
 *
 * @param save      Pointer to save function or NULL if not required
 * @param restore   Pointer to restore function or NULL if not required
 * @param ctx_size  Compile-time constant size of device context
 *
 * @return          Handle that identifies driver vis-a-vis the kernel
 */
#define syt_drv_register(save, restore, ctx_size) ({ \
	/* allocate context in checkpoint, therefore make it static */ \
    static __attribute__((section(".dev_ctx"))) \
	uint8_t __dev_ctx[ctx_size]; \
    \
	/* Prevent the compiler from optimizing the context away */ \
    __asm__ __volatile__("" :: "r" (&__dev_ctx)); \
    \
    /* Wrap internal register function to allocate context in checkpoint. Since \
     * we're in a macro, this will expand and allocate memory for every individual \
     * call by each driver. */ \
    int drv_register(drv_save_func_t save, drv_restore_func_t restore, size_t size); \
	drv_register(save, restore, ctx_size); \
})


/**
 * @brief Get pointer to the device context corresponding in "last" checkpoint
 *
 * @param handle    Handle provided by kernel
 * @return          Pointer to memory region in checkpoint reserved for context
 */
const void* syt_drv_get_ctx_last(int handle);


/**
 * @brief Get pointer to the device context corresponding in "next" checkpoint
 *
 * @param handle    Handle provided by kernel
 * @return          Pointer to memory region in checkpoint reserved for context
 */
void* syt_drv_get_ctx_next(int handle);


// Driver helpers ------------------------------------------------------------//
// These functions may help implementing partial dirtiness of device contexts to
// optimize save and restoration time. Each driver can choose to implement this.

/// Holds information about dirtiness, has to be passed to all related functions
typedef struct {
    void*  addr;    ///< address of dirty region (NULL if clean, SIZE_MAX if all dirty)
    size_t length;  ///< length of dirty region
} syt_dev_ctx_changes_t;


/**
 * @brief Mark context clean, so drv_save() won't do anything
 *
 * @param ctx_changes   Pointer to dirtiness information structure
 */
void drv_mark_clean(syt_dev_ctx_changes_t* ctx_changes);


/**
 * @brief Mark context dirty, so drv_save() will copy the whole context
 *
 * @param ctx_changes   Pointer to dirtiness information structure
 */
void drv_mark_dirty(syt_dev_ctx_changes_t* ctx_changes);


/**
 * @brief Mark context partially dirty, so drv_save() only copies dirty parts
 *
 * If multiple regions are marked dirty they will be merged into one region that
 * contains both, possibly including non-dirty parts.
 *
 * @param ctx_changes   Pointer to dirtiness information structure
 * @param addr
 * @param length
 */
void drv_dirty_range(syt_dev_ctx_changes_t* ctx_changes, void* addr, size_t length);


/**
 * @brief Copy dirty parts from one context to another if neccessary
 *
 * If @p ctx_from is dirty as indicated by @p ctx_changes, copy dirty parts to
 * @p ctx_to.
 *
 * @param ctx_changes   Pointer to dirtiness information structure
 * @param ctx_to
 * @param ctx_from
 * @param ctx_size
 */
void drv_save(syt_dev_ctx_changes_t* ctx_changes, void* ctx_to, void* ctx_from, size_t ctx_size);


/******************************************************************/
/********************************************** SYSTEM FUNCTIONS **/


// infinite loop with disabled interrupts and visual led signal (all leds on)
void syt_kernel_panic(void);

// returns the number of platform boot since the system start or since the last hard reset
uint16_t syt_stats_boot_count(void);

//returns the number of failed checkpoints since the system start or since the last hard reset
uint16_t syt_stats_failed_chkpt_count(void);

//returns the number of successfuly completed checkpoints since the system start or since the last hard reset
uint16_t syt_stats_completed_chkpt_count(void);

#endif // DRIVERS_UTILS_H
