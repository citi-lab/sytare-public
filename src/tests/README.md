# Automated tests

The tests in this folder are meant to be run on the target MSP430 and validated
by a connected debugger that checks the content of global variables.


## Conditions

For each test, after the program binary has been transfered to the target, a
sleeping time of approx. 1 second is granted for the test to pass. While the
tests execute, the target should be under intermittent power, i.e. it should be
power cycled.

**TODO:** Determine exact test conditions. Should they vary for different tests?


## Example

You have to connect a `MSP-EXP430FR5739` board to your computer such that
`mspdebug rf2500` can connect to the MCU. You can run each test individually:

```bash
$ mkdir dma/build
$ cd dma/build
$ cmake ..
$ make tests
...
[100%] Linking C executable tests_dma
[100%] Built target tests_dma
=== Transfering program to target
=== Running test for: tests_dma
stats_boot_count: PASSED
dst_memset: PASSED
dst_memcpy: PASSED
[100%] Built target tests
```

To run all tests:

```bash
$ ./run_tests.sh
```
