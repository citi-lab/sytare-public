#!/bin/bash

source ${0%/*}/../../../tools/elf_parse.sh

FILE=$1

assert_section $FILE syt_first_boot 		.os_data
assert_section $FILE syt_next				.os_bss

assert_section $FILE clk_device_context		.drv_data
# nothing in .drv_bss by default

assert_section $FILE a						.usr_data
assert_section $FILE b						.usr_bss

assert_section $FILE __syt_boot_start		.boot_text

assert_section $FILE syt_boot				.text

echo "== Section test passed, all symbols in their proper section"
