include(${CMAKE_CURRENT_LIST_DIR}/Sytare.cmake)
include_guard()

if(NOT SYTARE_KERNEL)
	set(SYTARE_APPLICATION TRUE)

	add_subdirectory(
		${CMAKE_CURRENT_LIST_DIR}/..
		${CMAKE_CURRENT_BINARY_DIR}/sytare)
endif()

# always link against sytare
link_libraries(sytare)
