set(CMAKE_SYSTEM_NAME Generic)
set(TARGET_ARCH msp430-elf)

if(TOOLCHAIN_BIN_DIR)
	# make sure the directory has a trailing slash
	set(TOOLCHAIN_BIN_DIR ${TOOLCHAIN_BIN_DIR}/)
endif()

set(CMAKE_C_COMPILER ${TOOLCHAIN_BIN_DIR}${TARGET_ARCH}-gcc)

# we cannot let CMake verify if the compiler works because it neeeds a linker
# script and such
set(CMAKE_C_COMPILER_WORKS 1 CACHE INTERNAL "")
