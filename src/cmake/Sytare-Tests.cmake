include(${CMAKE_CURRENT_LIST_DIR}/Sytare-Application.cmake)

# find name of test by its directory
get_filename_component(TEST_NAME ${CMAKE_CURRENT_SOURCE_DIR} NAME)
set(PROJECT_NAME tests_${TEST_NAME})

function(sytare_test PROJECT_NAME)
	# compile all C source files
	file(GLOB SRC *.c)
	file(GLOB EXPECT_FILE *.expect)

	add_executable(${PROJECT_NAME} ${SRC})
	target_program(${PROJECT_NAME})
	target_dump_listing(${PROJECT_NAME})

	add_custom_target(tests
		DEPENDS
			${PROJECT_NAME}
		COMMAND
			${CMAKE_COMMAND} -E echo "=== Transfering program to target"
		COMMAND
			mspdebug rf2500 'prog $<TARGET_FILE:${PROJECT_NAME}>' 2>/dev/null 1>/dev/null
		COMMAND
			${CMAKE_COMMAND} -E echo "=== Running test for: ${PROJECT_NAME}"
		COMMAND
			sleep 1
		COMMAND
			${SYTARE_TOOLS}/check_test.sh $<TARGET_FILE:${PROJECT_NAME}> ${EXPECT_FILE})
endfunction(sytare_test)
