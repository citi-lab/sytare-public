# Sytare CMake build system

## What has changed?

In constrast to the old `Makefile`s, CMake is a build-system generator, i.e.
when it's invoked, it will create a `Makefile`-based build system. So when you
run `cmake ..`, this is so-called *configuration-time* and when you run `make`,
it's *build-time*.

CMake mandates out-of-source builds, so you will have to create a build folder
for every application that you want to build.

You don't have to run `make` in multiple folders anymore to build your
application.

If you use some IDE, it might be that you're able to open a `CMakeLists.txt`
directly in your IDE the same way as you open a project, or you might be able to
import it. Either way, your IDE should then know all about the project without
further tuning.


## Building

```bash
$ cd demo/leds
$ mkdir build
$ cd build
$ echo 'Generate Makefile-based build system'
$ cmake ..
$ echo 'Build sytare'
$ make
```

You need the `msp430-elf` toolchain in order to build the project. It has to be
somewhere in your path in order for CMake to find it.

Verified to work with [msp430-gcc-6.2.1.16_linux64.tar.bz2](http://software-dl.ti.com/msp430/msp430_public_sw/mcu/msp430/MSPGCC/latest/index_FDS.html).


## Programming the dev board

In your applications `CMakeLists.txt` you can add the line

```
target_program(target_of_your_executable)
````

to be able to program the resulting executable directly via `make`:

```bash
$ make program-target_of_your_executable
```
