/**
 *  \file   dma.c
 *  \brief  TI FR5739 board lib, dma
 *  \author Tristan Delizy
 *  \date   2015
 **/

#include <stddef.h>
#include <msp430.h>
#include <drivers/dma.h>

#define DMATSEL_MASK (0x001FU)
#define DMA0TSEL_OFFSET (0)

void * dma_memset(void * dst, unsigned char val, size_t len)
{
    if(len == 0)
        return NULL;

    /* reset control register */
    DMA0CTL = 0x0;

    /* use DMAREQ as trigger*/
    DMACTL0 |= DMA0TSEL__DMAREQ;

    DMA0SA = (size_t) &val;
    DMA0DA = (size_t) dst;
    DMA0SZ = len;

    DMA0CTL = (DMADT_1      /* Block transfer mode     */
            | DMADSTINCR_3  /* Increment dst address   */
            | DMASRCINCR_0  /* Constant src address    */
            | DMAEN         /* Enable DMA              */
            | DMAREQ        /* Request DMA (runs copy) */
            | DMADSTBYTE    /* dst: byte unit          */
            | DMASRCBYTE);  /* src: byte unit          */

    return dst;
}


void * dma_memcpy(void * dst, const void * src, size_t len)
{
    unsigned int copymode = 0; /* word unit for dst and src */
    size_t dma0sz;
    void *initial_dst = dst;

    if(!len)
        return NULL;

    if(((size_t)dst & 0x1) && ((size_t)src & 0x1))
    {
        /* Both dst and src are odd, copy one byte and copy the rest */
        /* word-aligned                                              */
        *(char*)dst++ = *(char*)src++;
        --len;

        // word transfer
        dma0sz = len / 2;
    }
    else if(((size_t)dst & 0x1) || (size_t)src & 0x1)
    {
        /* dst and src have opposite parity, they cannot be word-copied. */
        /* Use byte copy instead                                         */
        copymode = DMADSTBYTE  /* dst: byte unit                         */
                 | DMASRCBYTE; /* src: byte unit                         */

        // byte transfer
        dma0sz = len;
    }
    else {
        // Any other case than the one above supports word transfer
        dma0sz = len / 2;
    }

    /* reset control register */
    DMA0CTL = 0x0;

    /* use DMAREQ as trigger*/
    DMACTL0 |= DMA0TSEL__DMAREQ;

    DMA0SA = (size_t) src;
    DMA0DA = (size_t) dst;
    DMA0SZ = dma0sz;

    DMA0CTL = (DMADT_1     /* Block transfer mode     */
            | DMADSTINCR_3 /* Increment dst address   */
            | DMASRCINCR_3 /* Increment src address   */
            | DMAEN        /* Enable DMA              */
            | DMAREQ       /* Request DMA (runs copy) */
            | copymode);   /* Byte or word transfer   */

    /* Copy the last byte, if necessary */
    /* copymode == 0 <=> word transfer  */
    if((copymode == 0) && (len & 0x1))
        *((char*)dst + len - 1) = *((char*)src + len - 1);

    return initial_dst;
}
