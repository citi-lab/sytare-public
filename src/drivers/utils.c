#include <stdint.h>
#include <stddef.h>
#include <drivers/dma.h>
#include <drivers/utils.h>

/// Mark the whole context as dirty by setting the address to this magic value
#define CTX_ALL_DIRTY   ((void*) SIZE_MAX)
/// Mark the whole context as clean by setting the address to this magic value
#define CTX_CLEAN       ((void*) NULL)

void
drv_mark_clean(syt_dev_ctx_changes_t* ctx_changes)
{
    ctx_changes->addr = CTX_CLEAN;
}


void
drv_mark_dirty(syt_dev_ctx_changes_t* ctx_changes)
{
    ctx_changes->addr = CTX_ALL_DIRTY;
}


void
drv_dirty_range(syt_dev_ctx_changes_t* ctx_changes, void* addr, size_t length)
{
    if(ctx_changes->addr == CTX_ALL_DIRTY) {
        // complete context is already dirty
    } else if(ctx_changes->addr == CTX_CLEAN) {
        // context is not yet dirty, set region
        ctx_changes->addr = addr;
        ctx_changes->length = length;
    } else {
        // context already dirty, extend range if necessary
        const size_t start  = min_uint( (size_t) addr,            (size_t) ctx_changes->addr);
        const size_t end    = max_uint(((size_t) addr) + length, ((size_t) ctx_changes->addr) + ctx_changes->length);
        ctx_changes->addr = (void*) start;
        ctx_changes->length = end - start;
    }
}


void
drv_save(syt_dev_ctx_changes_t* ctx_changes, void* ctx_to, void* ctx_from, size_t ctx_size)
{

    if(ctx_changes->addr == CTX_ALL_DIRTY) {
        // full save
        dma_memcpy(ctx_to, ctx_from, ctx_size);
    } else if(ctx_changes->addr == CTX_CLEAN) {
        // not dirty => no save
        return;
    } else {
        // calculcate offset of dirty region in ctx_from
        const size_t offset = (size_t) ctx_changes->addr - (size_t) ctx_from;

        // calculcate destination address in ctx_to
        void* const ctx_to_dst = (void*) ((size_t) ctx_to + offset);

        // partial save
        dma_memcpy(ctx_to_dst, ctx_changes->addr, ctx_changes->length);
    }

    // context is clean now
    drv_mark_clean(ctx_changes);
}
