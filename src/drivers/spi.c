/**
 *  \file   spi.c
 *  \brief  TI FR5739 board lib, spi
 *  \author Tristan Delizy
 *  \date   2015
 **/


/******************************************************************/
/****************************************************** INCLUDES **/

#include <msp430.h>
#include <drivers/spi.h>
#include <drivers/utils.h>
#include <drivers/port.h>
#include <drivers/dma.h>


/******************************************************************/
/********************************************** INTERNAL DEFINES **/

#define SPI_TX                      UCB0TXBUF
#define SPI_RX                      UCB0RXBUF

// SPI active polling
#define SPI_WAIT_EOT()              do { } while (! (UCB0IFG & UCTXIFG) )
#define SPI_WAIT_EOR()              do { } while (! (UCB0IFG & UCRXIFG) )

// SPI flags managment
#define SPI_CLR_RX_IFG()            do { UCB0IFG &= ~UCRXIFG; } while(0)


/******************************************************************/
/****************************************** MODULE CONFIGURATION **/

const SPI_SETTINGS spiSettings_default_config = {
    // control register 0
    ( UCCKPH        | /*Clock Phase*/
      UCMSB         | /*Most Significant Bit*/
      UCMST         | /*Master mode*/
      UCMODE_2      | /*4 wires with low active slave*/
      UCSYNC        | /*sync mode*/
      UCSSEL__SMCLK | /*source clock is system clock*/
      UCSTEM        ),/* 4th wire is used to control slave*/
    // baud rate control register
    0 /*no clock divider*/
};


/******************************************************************/
/********************************************************* TYPES **/

// sytare spi driver data descriptor for persistency
struct spi_device_context_t
{
    unsigned int    selected_slave;
    SPI_SETTINGS    settings;
};


/******************************************************************/
/***************************************************** VARIABLES **/

static
syt_dev_ctx_changes_t ctx_changes;

// local copy in RAM
static
struct spi_device_context_t spi_device_context;


/******************************************************************/
/***************************************************** FUNCTIONS **/

void spi_init(void)
{
    syt_drv_register(spi_drv_save, spi_drv_restore,
                    sizeof(struct spi_device_context_t));

    /* configure all SPI lines */
    // Chip Select for device 0
    prt_dir_bis(SPI_CS0_PORT, BITX(SPI_CS0_BIT));
    prt_sel0_bic(SPI_CS0_PORT, BITX(SPI_CS0_BIT));
    prt_sel1_bic(SPI_CS0_PORT, BITX(SPI_CS0_BIT));
    prt_out_bis(SPI_CS0_PORT, BITX(SPI_CS0_BIT));

    //... others slaves chip select configuration goes there

    // System clock
    prt_dir_bis(SPI_SCLK_PORT, BITX(SPI_SCLK_BIT));
    prt_sel0_bic(SPI_SCLK_PORT, BITX(SPI_SCLK_BIT));
    prt_sel1_bis(SPI_SCLK_PORT, BITX(SPI_SCLK_BIT));
    //PXSEL1(SPI_SCLK_PORT) |= BITX(SPI_SCLK_BIT);


    // Master Out Slave In (MOSI)
    prt_dir_bis(SPI_MOSI_PORT, BITX(SPI_MOSI_BIT));
    prt_sel0_bic(SPI_MOSI_PORT, BITX(SPI_MOSI_BIT));
    prt_sel1_bis(SPI_MOSI_PORT, BITX(SPI_MOSI_BIT));

    //Master In Slave Out (MISO)
    prt_dir_bic(SPI_MISO_PORT, BITX(SPI_MISO_BIT));
    prt_sel0_bic(SPI_MISO_PORT, BITX(SPI_MISO_BIT));
    prt_sel1_bis(SPI_MISO_PORT, BITX(SPI_MISO_BIT));

    // signal the state modification to the os
    drv_mark_dirty(&ctx_changes);

    return;
}

void spi_drv_save(int drv_handle)
{
    drv_save(&ctx_changes,
             syt_drv_get_ctx_next(drv_handle),
             &spi_device_context,
             sizeof(spi_device_context));
}


void spi_drv_restore(int drv_handle)
{
    dma_memcpy(&spi_device_context,
               syt_drv_get_ctx_last(drv_handle),
               sizeof(spi_device_context));

    spi_configure(&spi_device_context.settings);

    if(spi_device_context.selected_slave != SPI_NO_ACTIVE_SLAVE)
        spi_select_slave(spi_device_context.selected_slave);
    else
        spi_deselect_slave();

    // mark as dirty to make sure that it will be saved to the new checkpoint
    // that is still uninitialized after driver restoration
    drv_mark_dirty(&ctx_changes);
}


void spi_configure(SPI_SETTINGS const *cfg)
{
    // Start configuration by setting SPI module in reset state
    UCB0CTLW0 |= UCSWRST;

    /* configure the SPI controler registers */
    UCB0CTLW0 = (cfg->reg_CTLW0 | UCSWRST);
    UCB0BRW = (cfg->reg_BRW);

    // clear the reset state for enabling SPI
    UCB0CTLW0 &= ~UCSWRST;

    spi_device_context.settings.reg_CTLW0 = cfg->reg_CTLW0;
    spi_device_context.settings.reg_BRW = cfg->reg_BRW;

    // signal the state modification to the os
    drv_mark_dirty(&ctx_changes);

    return;
}

unsigned int spi_select_slave(unsigned int device)
{
    if(device > SPI_DEVICE_SUPPORTED-1) // slave id out of range
        return EBADSLAVE_SPI;

    if( (spi_device_context.selected_slave != SPI_NO_ACTIVE_SLAVE) &&
        (spi_device_context.selected_slave != device)) {
        // another slave is already active
        return EOTHERSLAVE_SPI;
    }

    spi_device_context.selected_slave = device;
    switch(spi_device_context.selected_slave){
        case 0 :prt_out_bic(SPI_CS0_PORT, BITX(SPI_CS0_BIT)); break;
        //... others slaves goes there
    }

    // signal the state modification to the os
    drv_mark_dirty(&ctx_changes);

    return ESUCCESS_SPI;
}

void spi_deselect_slave(void)
{
    switch(spi_device_context.selected_slave){
        case 0 :prt_out_bis(SPI_CS0_PORT, BITX(SPI_CS0_BIT)); break;
        //... others slaves goes there
    }
    spi_device_context.selected_slave = SPI_NO_ACTIVE_SLAVE;

    // signal the state modification to the os
    drv_mark_dirty(&ctx_changes);

    return;
}

unsigned int spi_is_recieving(void)
{
    return (prt_in_get(SPI_MISO_PORT) & BITX(SPI_MISO_BIT));
}

void spi_read_byte(unsigned char* dst)
{
    if(spi_device_context.selected_slave == SPI_NO_ACTIVE_SLAVE)
        return;

    SPI_TX  = SPI_DUMMY_BYTE;
    SPI_WAIT_EOR();
    *dst = SPI_RX;
}

void spi_read_frame(unsigned char* dst, unsigned int len)
{
    int i;

    if(spi_device_context.selected_slave == SPI_NO_ACTIVE_SLAVE || !len)
        return;

    for(i=0; i<len; i++)
    {
        SPI_TX  = SPI_DUMMY_BYTE;
        SPI_WAIT_EOR();
        dst[i] = SPI_RX;
    }
}

void spi_write_byte(const unsigned char* src)
{
    if(spi_device_context.selected_slave == SPI_NO_ACTIVE_SLAVE)
        return;

    SPI_CLR_RX_IFG();
    SPI_TX  = *src;
    SPI_WAIT_EOT();
}

void spi_write_frame(const unsigned char* src, unsigned int len)
{
    int i;

    if(spi_device_context.selected_slave == SPI_NO_ACTIVE_SLAVE || !len)
        return;

    for(i=0; i<len; i++)
    {
        SPI_CLR_RX_IFG();
        SPI_TX  = src[i];
        SPI_WAIT_EOT();
    }
}

void spi_transfert_byte(const unsigned char* src, unsigned char* dst)
{
    if(spi_device_context.selected_slave == SPI_NO_ACTIVE_SLAVE)
        return;

    SPI_CLR_RX_IFG();;
    SPI_TX = *src;
    SPI_WAIT_EOR();
    *dst = SPI_RX;
}

void spi_transfert_frame(const unsigned char* src, unsigned char* dst, unsigned int len)
{
    int i;

    if(spi_device_context.selected_slave == SPI_NO_ACTIVE_SLAVE || !len)
        return;

    for(i=0; i<len; i++)
    {
        SPI_CLR_RX_IFG();
        SPI_TX = src[i];
        SPI_WAIT_EOR();
        dst[i] = SPI_RX;
    }
}


